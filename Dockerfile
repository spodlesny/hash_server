FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y python3-dev python3-pip git

WORKDIR /opt
# Do not cache git clone
ADD https://www.unixtimestamp.com/ /tmp/bustcache
RUN mkdir -p ~/.ssh/
RUN ssh-keyscan -t rsa bitbucket.org > ~/.ssh/known_hosts
RUN git clone https://github.com/spodlesny/hash_server.git bakalarka

WORKDIR /opt/bakalarka
RUN pip3 install -Ur requirements.txt
RUN ln -s /opt/bakalarka/gunicorn.service /etc/systemd/system/gunicorn.service

RUN apt-get install -y nginx
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /opt/bakalarka/proxy_config.nginx /etc/nginx/sites-enabled/

CMD /usr/bin/python3 manage.py migrate && service nginx start && gunicorn --access-logfile - --workers 3 --bind unix:/opt/bakalarka/bakalarka.sock hash_server.wsgi:application