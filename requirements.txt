Django==2.1.7
django-bulma==0.5.6
django-debug-toolbar==1.11
gunicorn==19.9.0
psycopg2==2.7.6.1
pytz==2018.9
sqlparse==0.3.0
