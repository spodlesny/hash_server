from django.db import models
from django.db.models.functions import datetime
from django.utils import timezone


# Create your models here.


class HashList(models.Model):
    hash = models.CharField(max_length=1024, primary_key=True)
    hash_type = models.IntegerField(default=5500)
    cracked_value = models.CharField(null=True, blank=True, max_length=1024)
    status = models.CharField(max_length=32, default="New")
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} ({})".format(self.hash, self.cracked_value)


class HashcatControl(models.Model):
    statuses = (("DOWN", "Hashcat is not running."),
                ("TIMEOUT", "Hashcat is not responding."),
                ("UP", "Hashcat is running."),
                ("STOP", "Hashcat has been stopped."),
                ("RESTART", "Hashcat is being restarted."),
                ("NOT YET INITIALIZED", "Hashcat did not connect yet."))
    status = models.CharField(choices=statuses, default=statuses[-1][0], max_length=256)
    error = models.BooleanField(default=False)
    commands = (
        ("start", "start"),
        ("stop", "stop"),
        ("restart", "restart"),
    )
    command = models.CharField(choices=commands, default=commands[0][0], max_length=256)
    error_response = models.CharField(max_length=1024, blank=True, null=True)
    stdout = models.TextField(default="")
    stderr = models.TextField(default="")
    last_ping = models.DateTimeField(default=datetime.datetime.now)

class NetCredsOutput(models.Model):
    data = models.TextField(default="")