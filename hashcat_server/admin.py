from django.contrib import admin

# Register your models here.
from hashcat_server.models import HashList, HashcatControl, NetCredsOutput

admin.site.register(HashList)
admin.site.register(HashcatControl)
admin.site.register(NetCredsOutput)