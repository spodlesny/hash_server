import datetime
import json
import re

from django.core.serializers import serialize
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone

# Create your views here.
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView, ListView, DetailView

from hashcat_server.models import HashList, HashcatControl, NetCredsOutput


class ShowTableOfHashes(ListView):
    model = HashList
    template_name = 'table_of_hashes.html'
    ordering = ['-timestamp']

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ShowTableOfHashes, self).get_context_data(**kwargs)
        try:
            context["hashcat_status"] = HashcatControl.objects.last().get_status_display()
            context["last_ping"] = HashcatControl.objects.last().last_ping
            context["ordered"]  = HashcatControl.objects.last().get_command_display()
        except AttributeError as err:
            context["hashcat_status"] = None
        try:
            # remove ANSI escaping (solution from: https://stackoverflow.com/a/14693789)
            ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
            stdout = HashcatControl.objects.last().stdout
            clean_stdout = ansi_escape.sub('', stdout)
            context["stdout"] = clean_stdout
        except AttributeError:
            context["stdout"] = None

        return context


class RogueAPCredentials(ListView):
    model = NetCredsOutput
    template_name = 'net-creds_output.html'


@csrf_exempt
def save_new_value(request):
    if request.method == "POST":
        request_data = json.loads(request.body)
        if request_data.get('hash') and request_data.get('hash_type'):
            row = HashList.objects.create(hash=request_data.get('hash'),
                                          hash_type=request_data.get('hash_type'))
            row.save()
            return HttpResponse(status=200)
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=405)


@csrf_exempt
def edit_hash_status(request):
    if request.method == "POST":
        request_data = request.POST
        if request_data.get('hash') and request_data.get('status') and request_data.get('hash_type'):
            row, created = HashList.objects.get_or_create(hash=request_data.get('hash'),
                                                          hash_type=request_data.get('hash_type'))
            print(created)
            row.status = request_data.get('status')
            row.save()
            return HttpResponseRedirect(reverse('hashcat_sever:hashes'))
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=405)


@csrf_exempt
def remove_hash_from_list(request):
    if request.method == "POST":
        request_data = request.POST
        if request_data.get('hash') and request_data.get('hash_type'):
            row = HashList.objects.get(hash=request_data.get('hash'), hash_type=request_data.get('hash_type'))
            row.delete()
            return HttpResponseRedirect(reverse('hashcat_sever:hashes'))
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=405)

@csrf_exempt
def hashcat_action(request):
    if request.method == "POST":
        request_data = request.POST
        if request_data.get('action'):
            row = HashcatControl.objects.last()
            if row is None:
                row = HashcatControl.objects.create()
                print("First init for: HashcatControl")
            row.command = request_data.get('action')
            row.save()
            return HttpResponseRedirect(reverse('hashcat_sever:hashes'))
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=405)


def check_hashcat_command(request):
    if request.method == "GET":
        data = HashcatControl.objects.last()
        if data is None:
            HashcatControl.objects.create()
            data = HashcatControl.objects.last()
            print("First init for: HashcatControl")

        json_data = serialize('json', [data, ])

        # Update time
        data.last_ping = timezone.now()
        data.save()
        return HttpResponse(json_data, content_type="application/json")
    else:
        return HttpResponse(status=405)


def get_hashes_from_queue(request):
    if request.method == "GET":
        data_set = HashList.objects.filter(status="In queue")
        json_data = serialize('json', data_set)
        return HttpResponse(json_data, content_type="application/json")
    else:
        return HttpResponse(status=405)

@csrf_exempt
@require_http_methods(["POST"])
def edit_hash_status_using_json(request):
    json_data = json.loads(request.body)
    hash_value = json_data.get("hash")
    new_status = json_data.get("status")
    hash_object = HashList.objects.get(hash=hash_value)
    hash_object.status = new_status
    hash_object.save()
    return HttpResponse(status=200)

@csrf_exempt
@require_http_methods(["POST"])
def add_hash_value(request):
    json_data = json.loads(request.body)
    hash_value = json_data.get("hash")
    cracked_value = json_data.get("value")
    hash_object = HashList.objects.get(hash=hash_value)
    hash_object.cracked_value = cracked_value
    hash_object.save()
    return HttpResponse(status=200)

@csrf_exempt
def change_hashcat_status(request):
    if request.method == "POST":
        data = json.loads(request.body)
        hashcat_control = HashcatControl.objects.last()
        if data.get("status"):
            hashcat_control.status = data.get('status')
            if data.get("status") == "RESTART":
                hashcat_control.command = "start"
            hashcat_control.save()

        hashcat_control.save()
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=405)


@csrf_exempt
@require_http_methods(["POST"])
def save_netcreds_output(request):
    if b"Starting net-creds.py" in request.body:
        NetCredsOutput.objects.create(data=request.body.decode())
    else:
        row = NetCredsOutput.objects.last()

        # remove ANSI escaping (solution from: https://stackoverflow.com/a/14693789)
        ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
        new_data = request.body.decode()
        clean_new_data = ansi_escape.sub('', new_data)

        row.data = row.data+clean_new_data
        row.save()
    return HttpResponse(status=200)
