from django.conf.urls import url

from hashcat_server import views

app_name = "hashcat_sever"

urlpatterns = [
    url(r'^$', views.ShowTableOfHashes.as_view(), name='hashes'),
    url(r'^credentials$', views.RogueAPCredentials.as_view(), name='rogue_ap'),
    url(r'^new_hash$', views.save_new_value),
    url(r'^get_hashes_from_queue$', views.get_hashes_from_queue),
    url(r'^edit_hash_status$', views.edit_hash_status_using_json),
    url(r'^add_hash_value$', views.add_hash_value),
    url(r'^change_hashcat_status$', views.change_hashcat_status),
    url(r'^edit_status$', views.edit_hash_status, name="edit_hash"),
    url(r'^remove_hash$', views.remove_hash_from_list, name="remove_hash"),
    url(r'^hashcat_control$', views.check_hashcat_command, ),
    url(r'^hashcat_action$', views.hashcat_action, name="hashcat_action"),
    url(r'save_credentials', views.save_netcreds_output)
]
