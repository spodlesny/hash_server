from django.apps import AppConfig


class HashcatServerConfig(AppConfig):
    name = 'hashcat_server'
